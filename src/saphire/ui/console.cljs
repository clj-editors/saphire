(ns saphire.ui.console
  (:require [tango.ui.console :as tango-console]
            [tango.ui.elements :as ui]
            [clojure.string :as str]))

(defn- re-escape [string]
  (str/replace string #"[\|\\\{\}\(\)\[\]\^\$\+\*\?\.\-\/]" "\\$&"))

(defn- summarize-path [state path]
  (let [{:keys [get-config]} (:editor/callbacks @state)
        paths (:project-paths (get-config))
        [matching] (filter #(str/starts-with? path %) paths)
        pattern (when matching
                  (-> matching
                      re-escape
                      (str "[/\\\\]?")
                      re-pattern))
        relative (cond-> path matching (str/replace-first  pattern ""))
        sep (if (str/starts-with? (first paths) "/")
              "/"
              "\\")
        splitted (str/split relative sep)
        [prefix [file]] (split-at (-> splitted count dec) splitted)]
    (str (str/join sep (map #(subs % 0 1) prefix))
         sep file)))

(defn- append-trace! [state console file row]
  (let [open-editor (-> @state :editor/callbacks :open-editor)
        traces (.querySelector console "div.traces")
        crumbs (.-children traces)
        txt (str (summarize-path state file) ":" (inc row))
        a (doto (js/document.createElement "a")
                (.setAttribute "href" "#")
                (-> .-innerText (set! txt))
                (-> .-onclick (set! (fn [^js evt]
                                      (.preventDefault evt)
                                      (open-editor {:file-name file
                                                    :line row})))))
        span (js/document.createElement "span")]
    (when (-> crumbs .-length (> 0))
      (.append span " > "))
    (.appendChild span a)
    (.appendChild traces span)))

(defn update-traces! [state]
  (let [traces (:repl/tracings @state)
        con ((:get-console (:editor/callbacks @state)))
        query (.. con (querySelector "input.search-trace") -value)
        query2 (.. con (querySelector "input.search-trace-not") -value)
        reg (try (re-pattern query) (catch :default _ #""))
        reg2 (try (some-> (not-empty query2) re-pattern) (catch :default _ #""))
        filtered (filter #(re-find reg (:file %)) traces)
        removed (cond->> filtered reg2 (remove #(re-find reg2 (:file %))))
        traces (take-last 20 removed)]
    (set! (.. con (querySelector "div.traces") -innerText) "")
    (doseq [{:keys [file row]} traces]
      (append-trace! state con file row))))

(defn update-watch!
  "Updates a watch expression. It `watch` parameter contains:
* `:id` - some ID that identifies the watch expression
* `:text` - a text that will be displayed in the watch expression
* `:callback` - a callback that will be called when we click the button. If `nil`, no button will
  be displayed, and `icon` will be just a badge with the icon
* `:icon` - an icon for the button
* `:file` - the file being watched
* `:row` - a 0-based row

This code will ALWAYS update the watch with the same ID. If none is found, a new one will be
created. If you only send the `:id`, the watch element for that ID will be removed"
  [state {:keys [id text] :as watch}]
  (let [console ((:get-console (:editor/callbacks @state)))
        open-editor (-> @state :editor/callbacks :open-editor)
        chars (count text)
        text (if (< chars 55)
               text
               (str "..." (subs text (- chars 55))))
        link-elem (delay
                   (doto (js/document.createElement "a")
                         (-> .-innerText (set! text))
                         (.setAttribute "href" "#")
                         (-> .-onclick (set! (fn [evt]
                                               (.preventDefault evt)
                                               (open-editor {:file-name (:file watch)
                                                             :line (:row watch)}))))))
        icon (delay
              (doto (js/document.createElement "button")
                    (.. -classList (add "btn" "icon" (:icon watch)))
                    (-> .-onclick (set! (:callback watch)))))
        parent-elem (. console querySelector (str ".watches *[data-id=" id "]"))]

    (if (-> watch keys (= [:id]))
      (when parent-elem (set! (.-outerHTML parent-elem) ""))
      (let [parent-elem (or parent-elem
                            (doto (js/document.createElement "div")
                                  (.setAttribute "data-id" id)))
            watch-elem (. console querySelector ".watches")]
        (set! (.-innerHTML parent-elem) "")
        (cond
          (:callback watch)
          (. parent-elem appendChild @icon)

          (:icon watch)
          (.. @link-elem -classList (add "inline-block" "status-ignored" "icon" (:icon watch))))
        (. parent-elem appendChild @link-elem)
        (. watch-elem appendChild parent-elem)))))

(defn- clear-file-watches [repl-state]
  (when-let [filename (-> @repl-state :repl/watch.info :file)]
    (let [remove-watches (-> @repl-state :editor/features :remove-watches)]
      (remove-watches {:file filename}))))

(defn prepare! [^js c repl-state]
  (let [contents [:<>
                  [:div {:class "title"} "Trace"]
                  [:div {:class "cols"}
                   [:input {:class ["search-trace" "input-text"] :placeholder "Only"
                            :onchange #(update-traces! repl-state)}]
                   [:input {:class ["search-trace-not" "input-text"] :placeholder "Exclude"
                            :onchange #(update-traces! repl-state)}]]
                  [:div {:class "traces"}]
                  [:div {:class "title"} "Watch Points "
                   [ui/Link {:icon ["icon" "icon-x"] :on-click #(clear-file-watches repl-state)}]]
                  [:div {:class "watches"}]]]
    (.. c (querySelector ".details") (appendChild (ui/dom contents)))
    (tango-console/clear c)))
