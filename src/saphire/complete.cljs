(ns saphire.complete
  (:require [clojure.string :as str]
            [promesa.core :as p]
            [saphire.code-treatment :as treat]
            [orbit.evaluation :as eval]))

(defn suggestions [state]
  (p/let [dissected (treat/dissect-editor-contents state)
          watch-id (treat/watch-id-for-code state)
          res (treat/eql state [:repl/evaluator
                                {:editor/contents [:editor/filename :text/range]}])
          evaluator (:repl/evaluator res)
          editor-info (:editor/contents res)
          method-code (str "binding.local_variables.map { |m| ['local_var', m.to_s] } + "
                           "__self__.public_methods.map { |m| ['pub_method', m.to_s] } + "
                           "__self__.protected_methods.map { |m| ['prot_method', m.to_s] }")
          reg (apply str (interpose ".*" (:identifier dissected)))
          norm-symbol (str "Symbol.all_symbols.lazy.select { |x| x.inspect =~ /" reg "/ }"
                           ".select { |x| x.inspect !~ /\\\\x/ }")
          code (case (:type dissected)
                 "constant" "::Object.constants.map { |i| ['constant', i.to_s] }"
                 "instance_variable" "instance_variables.map { |i| ['instance_var', i.to_s] }"
                 "class_variable" "class_variables.map { |i| ['class_var', i.to_s] }"
                 "identifier" (str (str/replace method-code #"__self__\." "")
                                   " + " norm-symbol ".map { |x| ['symbol', x.to_s + ':'] }.to_a")
                 "scope_resolution" (str (:callee dissected) ".constants.map { |i| ['constant', i.to_s] }")
                 "call" (if (-> dissected :sep (= "::"))
                          (str (:callee dissected) ".constants.map { |i| ['constant', i.to_s] }")
                          (str/replace method-code #"__self__" (:callee dissected)))
                 "simple_symbol" (str norm-symbol ".map { |x| ['symbol', ':' + x.to_s] }.to_a"))]

    (p/catch
     (try (eval/evaluate evaluator
                         (cond-> {:code code
                                  :file (-> editor-info :editor/filename)
                                  :line (-> editor-info :text/range ffirst)}
                           watch-id (assoc :watch_id watch-id))
                         {:plain true :options {:op "eval"}})
       (catch :default _))
     (constantly nil))))


#_
(p/let [a
        (eval/evaluate evaluator
                       (cond-> {:code (str "Symbol.all_symbols.lazy"
                                           ".select { |x| x =~ /p.*a.*s.*s.*w.*o.*r.*d.*c.*o.*n.*f/ }"
                                           ".select { |x| x.inspect !~ /\\\\x/ }"
                                           ".map { |x| ['symbol', ':' + x.to_s] }.to_a ")})
                       {:plain true :options {:op "eval"}})]
  (count a))
