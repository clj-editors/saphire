(ns saphire.code-treatment
  (:require [clojure.string :as str]
            [promesa.core :as p]
            [tango.commands-to-repl.pathom :as pathom]
            [orbit.evaluation :as eval]
            [com.wsscode.pathom3.connect.operation :as connect]
            [duck-repled.tree-sitter :as ts]
            ["path" :as path]))

; (def ts-path (path/join js/__dirname "tree-sitter"))
; (defonce Parser (js/require ts-path))
; (prn :TS ts-path)
; (defonce initialized
;   (.init Parser #js {:locateFile #(path/join js/__dirname "tree-sitter.wasm")}))
(def Parser ts/Parser)
(def initialized ts/initialized)

(def done
  (p/do!
   initialized
   (p/let [my-path js/__dirname
           ruby (.. Parser -Language (load (path/join my-path "ruby.wasm")))]
     (def ruby-language ruby)
     (def parser (new Parser))
     (.setLanguage parser ruby-language)
     (def body-query (.query ruby "
(method  (body_statement) @body)
(singleton_method  (body_statement) @body)"))
     (def ^js parent-query (.query ruby "
(module) @parent (class) @parent
(method) @method (singleton_method) @singleton"))
     (def ^js method-name-query (.query ruby (str "(method name: (_) @name) @method "
                                                  "(singleton_method name: (_) @name) @method")))
     (def any-query (.query ruby "(_) @any"))
     (def identifier-query (.query ruby "(call) @call (identifier) @identifier"))
     (def call-query (.query ruby "(call) @call"))
     (def ctx-query (.query ruby "
(identifier) @identifier
(constant) @identifier
(instance_variable) @identifier
(class_variable) @identifier
(call (argument_list) @arglist)

\".\" @sep
\"::\" @sep
\":\" @sep
\"[\" @sep
\"]\" @sep
\"(\" @sep
\")\" @sep
\"{\" @sep
\"}\" @sep
")))))

(defn- dissect-node [node]
  (let [myself (.-text node)
        my-type (.-type node)
        parent (.-parent node)
        parent-type (.-type parent)
        [callee sep caller params?] (.-children parent)
        my-children (delay (mapv (fn [^js node]
                                   [(.-text node)
                                    (.-type node)])
                                 (.-children node)))
        callee-txt (.-text callee)
        assign (fn [element ^js val]
                 (if (#{"operator_assignment" "assignment"} (.-type val))
                   (let [[l a] (.-children val)]
                     (assoc element
                            :assign/expression (str (.-text l) " " (.-text a))
                            :assign/left-side (.-text l)))
                   element))
        params (fn [params]
                 (when-let [params (some-> params .-text)]
                   (if (str/starts-with? params "(")
                     params
                     (str " " params))))
        flat-scope? (and (= "scope_resolution" parent-type)
                         (nil? caller))]

    (cond
      (#{"call" "scope_resolution"} my-type)
      (assign {:identifier ""
               :type my-type
               :sep (-> @my-children second first)
               :callee (ffirst @my-children)
               :callee-type (ffirst @my-children)}
              parent)

      flat-scope?
      {:identifier (str "::" myself) :type my-type}

      (= "scope_resolution" parent-type)
      (assign {:identifier (.-text caller)
               :type "scope_resolution"
               :sep (.-text sep)
               :callee callee-txt
               :callee-type (.-type callee)}
              (.-parent parent))

      (or (not= "call" parent-type) (= myself callee-txt))
      (assign {:identifier myself
               :type my-type}
              parent)

      caller
      (assign {:identifier (.-text caller)
               :type "call"
               :params (params params?)
               :sep (.-text sep)
               :callee callee-txt
               :callee-type (.-type callee)}
              (.-parent parent))

      callee
      (assign {:identifier (.-text callee)
               :type "call"
               :params (params sep)}
              (.-parent parent))

      :else
      {:identifier myself :type my-type})))

#_
(->> captures
     (map #(.-node %))
     butlast
     last
     .-children)
     ; (map #(.-text %)))
#_
(->> captures
     (map #(.-node %))
     ; (map (fn [%] [(.-type %) (.-text %)]))
     last
     dissect-node
     ,,,)

(defn- range-contains? [[e-row e-col] ^js end]
  (def end end)
  (let [inside-row? (delay (< (.. end -startPosition -row) e-row (.. end -endPosition -row)))
        same-row? (delay (or (= e-row (.. end -startPosition -row)) (= e-row (.. end -endPosition -row))))
        inside-col? (delay (<= (.. end -startPosition -column) e-col (.. end -endPosition -column)))]
    (if @same-row?
      @inside-col?
      @inside-row?)))

#_
(let [[e-row e-col] [1 2]
      end (clj->js {:start {:row 0 :column 2} :end {:row 1 :column 1}})]
  (let [inside-row? (delay (< (.. end -start -row) e-row (.. end -end -row)))
        same-row? (delay (or (= e-row (.. end -start -row)) (= e-row (.. end -end -row))))
        inside-col? (delay (<= (.. end -start -column) e-col (.. end -end -column)))]
    (if @same-row?
      @inside-col?
      @inside-row?)))

(defn dissect-editor-contents
  ([state]
   (p/let [eql (-> @state :editor/features :eql)
           {:keys [editor/contents]} (eql [:editor/contents])]
     (dissect-editor-contents (:text/contents contents) (:text/range contents))))
  ([text range]
   (let [[[r-start c-start] end] range
         [r-end c-end] end
         ^js parsed (.parse parser text)
         ^js q any-query
         captures (.captures q (.-rootNode parsed)
                             #js {:row r-start :column (dec c-start)}
                             #js {:row r-end :column c-end})
         last-node (->> captures
                        ; (map #(.-node %))
                        ; (filter #(range-contains? end %))
                        last
                        .-node)
         parent (.-parent last-node)
         dissected (if (-> parent .-type (= "right_assignment_list"))
                     {:type "literal"
                      :identifier (.. parent -parent -text)}
                     (dissect-node last-node))]
     (.delete parsed)
     dissected)))

(defn- translate-captures-into-method [captures-res]
  (let [[parents [capture]] (split-with #(= "parent" (.-name %)) captures-res)]
    (when capture
      (let [^js node (.-node capture)
            texts (map (fn [parent]
                         (let [[kind name] (.. parent -node -children)]
                           (str (.-text kind) " " (.-text name))))
                       parents)
            parent-name (str/replace (str/join "::" texts) #"(class|module) " "")
            original-row (-> node .-startPosition .-row)
            is-method? (-> capture .-name (= "method"))
            method-part (-> node .-children (nth 1) .-text)]
        {:row original-row
         :method (str parent-name
                      (if is-method? "#" ".")
                      method-part)}))))

(defn method-name [source row]
  (let [^js parsed (.parse parser source)
        ^js query parent-query
        captures-res (. query captures (.-rootNode parsed) #js {:row row :column 0})
        result (when (seq captures-res) (translate-captures-into-method captures-res))]
    (.delete parsed)
    result))

(defn eql [state query]
   (p/let [eql (-> @state :editor/features :eql)
           result (eql (if (keyword? query) [query] query))]
     (if (keyword? query)
       (query result)
       result)))

(defn watch-id-for-code
  ([state]
   (p/let [eql (-> @state :editor/features :eql)
           {:keys [editor/contents]} (eql [:editor/contents])]
     (watch-id-for-code state (:text/contents contents) (-> contents :text/range ffirst))))
  ([state source row]
   (let [{:keys [method]} (method-name source row)]
     (get-in @state [:repl/watch method :watches 0]))))

(defn- make-parent [^js editor ^js query [[row col]]]
  (let [range #js {:row row :column col}
        captures (. query captures (.. editor -languageMode -tree -rootNode)
                   range range)]
    {:nodes captures
     :parents (reduce (fn [acc capture]
                        (let [[kind name] (.. capture -node -children)]
                          (if (-> capture .-name (= "parent"))
                            (conj acc (str (.-text kind) " " (.-text name)))
                            acc)))
                      []
                      captures)}))

(connect/defresolver completions [{:keys [editor/data repl/evaluator]}]
  {::connect/output [:completions/var]}

  (when (-> data :language (= :ruby))
    (p/let [^js editor (:editor data)
            {:keys [nodes parents]} (make-parent editor parent-query (:range data))
            ^js last-node (last nodes)
            parents (cond-> parents
                      (some-> nodes last .-name (= "parent")) butlast)
            inner-code (if (some-> last-node .-node .-type (#{"method" "singleton"}))
                         "instance_methods"
                         "methods")
            inner-code (str/replace (str "( Class.instance_method(:M).bind(self).call +"
                                         " Class.instance_method(:private_M).bind(self).call +"
                                         " Class.instance_method(:protected_M).bind(self).call ).map(&:to_s)")
                                    #"M"
                                    inner-code)
            contents (str (str/join "\n" parents)
                          "\n" inner-code "\n"
                          (str/join "\n" (repeat (count parents) "end")))
            result (eval/evaluate evaluator contents {:filename (:filename data)
                                                      :row (-> data :range first first)
                                                      :plain true})]
      {:completions/var (mapv (fn [candidate]
                                {:text/contents candidate
                                 :completion/type :function})
                              result)})))

(defn prepare-modified-top-block [data]
  (p/let [^js editor (:editor data)
          _ (.. editor -languageMode atTransactionEnd)
          {:keys [nodes parents]} (make-parent editor parent-query (:range data))
          ^js last-node (if-let [node (last nodes)]
                          (.-node node)
                          (-> (make-parent editor any-query (:range data)) :nodes (nth 1) .-node))
          parents (cond-> parents
                    (some-> nodes last .-name (= "parent")) butlast)]
    {:parents parents
     :last-node last-node}))

(connect/defresolver current-var [{:text/keys [language contents range]}]
  {::connect/output [:text/current-var]}
  (when (= language :ruby)
    (let [dissected (dissect-editor-contents contents range)]
      (when-let [method (case (:type dissected)
                          "identifier" (:identifier dissected)
                          "call" (str (:callee dissected) (:sep dissected) (:identifier dissected))
                          nil)]
        {:text/current-var {:text/contents method}}))))

(connect/defresolver doc-for-var [{:keys [text/language editor/data repl/evaluator]}]
  {::connect/output [:render/doc]}
  (when (= language :ruby)
    (let [current-text (:contents data)
          dissected (dissect-editor-contents current-text (:range data))]
      (when-let [find-method (case (:type dissected)
                               "identifier" (str "method(:" (:identifier dissected) ")")
                               "call" (str (:callee dissected) ".method(:" (:identifier dissected) ")")
                               nil)]
        (p/let [code (str "proc {"
                          "\n require 'rdoc'"
                          "\n  m = " find-method
                          "\n  c = m.owner"
                          "\n  name = m.original_name.to_s"
                          "\n  canonical_name = if c.singleton_class?"
                          "\n    c = c.attached_object"
                          "\n    break unless c.instance_of?(::Class)"
                          "\n    \"#{c}.#{name}\""
                          "\n  else"
                          "\n    \"#{c}##{name}\""
                          "\n  end"
                          "\n  ri = RDoc::RI::Driver.new"
                          "\n  name = ri.expand_name(canonical_name)"
                          "\n  filtered = ri.lookup_method(name)"
                          "\n  clj = Class.new {"
                          "\n    def initialize(n); @n = n; end"
                          "\n    def inspect; 'ui/' + @n.to_s; end"
                          "\n  }"
                          "\n  visitor = Class.new {"
                          "\n    attr_reader :hiccup"
                          "\n    def initialize; @hiccup = []; end"
                          "\n    define_method(:method_missing) { |name, arg|"
                          "\n      case name"
                          "\n      when :accept_heading then @hiccup << [clj.new(:Title), arg.text]"
                          "\n      when :accept_blank_line then @hiccup << clj.new(:Space)"
                          "\n      when :accept_paragraph"
                          "\n        children = []"
                          "\n        arg.parts.each { |part|"
                          "\n          if part.is_a?(String)"
                          "\n            children << part"
                          "\n          end"
                          "\n        }"
                          "\n        @hiccup << children.unshift(clj.new(:Markdown))"
                          "\n      when :accept_verbatim"
                          "\n        @hiccup << [clj.new(:Markdown), \"```ruby\n#{arg.parts.join('')}\\n```\"]"
                          "\n      when :accept_list_item_start"
                          "\n        if(arg.label)"
                          "\n          arg.label.each { |x| @hiccup << [clj.new(:Title), x] }"
                          "\n        end"
                          "\n      end"
                          "\n    }"
                          "\n  }.new"
                          "\n  ri.method_document(name, filtered).parts.each { |p| p.accept(visitor) }"
                          "\n  visitor.hiccup"
                          "\n}.call")

                res (eval/evaluate evaluator
                                   code
                                   {:filename (:filename data)
                                    :row (-> data :range first first)
                                    :plain true :no-wrap true})]
          {:render/doc {:html (->> res (cons 'ui/Rows) vec)}})))))

(defn- contextualize-method [parents ^js capture]
  (let [parents-str (reduce (fn [acc capture]
                             (let [[kind name] (.. capture -node -children)]
                               (conj acc (str (.-text kind) " " (.-text name)))))
                     []
                     parents)
        last-node (.-node capture)
        start-pos (.-startPosition last-node)
        end-pos (.-endPosition last-node)
        num-parents (count parents-str)]
    [(str (str/join "\n" parents-str)
          "\n" (-> capture .-node .-text) "\n"
          (str/join "\n" (repeat num-parents "end")))
     [[(- (.-row start-pos) num-parents) 0]
      [(+ 0 (.-row end-pos)) 3]]]))
