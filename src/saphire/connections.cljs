(ns saphire.connections
  (:require [clojure.string :as str]
            [orbit.evaluation :as eval]
            [tango.integration.connection :as conn]
            [tango.ui.edn :as edn]
            [tango.ui.console :as tango-console]
            [saphire.ui.console :as saphire-console]
            [promesa.core :as p]
            [tango.commands-to-repl.pathom :as pathom]
            [saphire.code-treatment :as treat]
            [saphire.ui.ruby :as ui-ruby]
            [saphire.ruby-parsing :as rp]
            [com.wsscode.pathom3.connect.operation :as connect]
            [tango.ui.elements :as ui]
            [tango.state :as state]
            ["path" :as path]
            ["fs" :as fs]))

(def ^:private unused-cmds
  #{:run-test-for-var :run-tests-in-ns :load-file :go-to-var-definition})

#_
(defn- update-breakpoint! [state result hit?]
  (let [console ((:get-console (:editor/callbacks @state)))
        file (:file/filename result)
        file-chars (count file)
        row (-> result :text/selection :text/range ffirst)
        file-text (if (< file-chars 35)
                    file
                    (str "..." (subs file (- file-chars 35))))
        link-text (str " " file-text ":" (inc row))
        link-elem (doto (js/document.createElement "a")
                        (-> .-innerText (set!
                                          (if hit?
                                            link-text
                                            (str link-text " (...waiting...)"))))
                        (-> .-onclick (set! (fn [evt]
                                              (.preventDefault evt)
                                              (.. js/atom -workspace (open file
                                                                           #js {:initialLine row
                                                                                :searchAllPanes true}))))))
        icon (doto (js/document.createElement "button")
                   (.. -classList (add "btn" "icon" "icon-playback-play")))]

    (swap! state assoc :repl/breakpoint
           {:file (:file/filename result), :row row})
    (doto (. console querySelector ".breakpoint")
          (-> .-innerText (set! ""))
          (. appendChild icon)
          (. appendChild link-elem))))

#_
(defn- clear-old-breakpoint [state]
  (when-let [old-breakpoint (:repl/breakpoint @state)]
    (let [console ((:get-console (:editor/callbacks @state)))]
      (eval/evaluate (:repl/evaluator @state)
                     {}
                     {:plain true
                      :options {:op "eval_resume"}})
      (set! (.. console (querySelector ".breakpoint") -innerHTML) ""))))

(defn- wrap-node [parents ^js capture]
  (let [node (.-node capture)
        texts (map (fn [parent]
                     (let [[kind name] (.-children parent)]
                       (str (.-text kind) " " (.-text name))))
                   parents)
        [node-def [node-body possible-body]] (->> node
                                                  .-children
                                                  (split-with #(not (contains? #{"body_statement" "="} (.-type %)))))
        def-text (->> node-def
                      (map #(.-text ^js %))
                      (str/join " "))
        node-children (cond
                        (nil? node-body) ["nil"]

                        (= "body_statement" (.-type node-body))
                        (->> node-body .-children (mapv #(.-text %)))

                        :single-line-method
                        [(.-text possible-body)])
        parent-name (str/replace (str/join "::" texts) #"(class|module) " "")
        method-name (str parent-name
                         (if (-> capture .-name (= "method")) "#" ".")
                         (->> node-def rest (filter #(-> % .-type (= "identifier"))) first .-text))
        node-lines (cons (str "NREPL.watch!(binding, " (pr-str method-name) ")") node-children)
        num-parents (count parents)
        original-row (-> node .-startPosition .-row)]
    {:row original-row
     :adapted-row (- original-row num-parents)
     :method method-name
     :text (str (str/join "\n" texts)
                "\n" def-text "\n" (str/join "\n" node-lines) "\nend\n"
                (str/join "\n" (repeat num-parents "end")))}))

(defn- get-all-watches [state]
  (p/let [eql (-> @state :editor/features :eql)
          data (p/-> (eql [:editor/data]) :editor/data)
          ^js parsed (.parse treat/parser (:contents data))
          captures-res (. treat/parent-query captures (.-rootNode parsed))

          res
          (->> captures-res
               (reduce (fn [{:keys [parents texts] :as acc} ^js capture]
                         (let [last-parent (some-> (not-empty parents) peek)
                               start-index (.. capture -node -startIndex)
                               parents (->> parents
                                            (filter #(<= start-index (.-endIndex ^js %)))
                                            vec)
                               acc (assoc acc :parents parents)]
                           (if (-> capture .-name (= "parent"))
                             (update acc :parents conj (.-node capture))
                             (update acc :texts conj (wrap-node parents capture)))))
                       {:parents [] :texts []})
               :texts)]
    (.delete parsed)
    res))

(defn- to-id [old-id]
  (-> old-id
      (str/replace #"#" "_HASH_")
      (str/replace #"/" "_SLASH_")
      (str/replace #"\\" "_BSLASH_")
      (str/replace #":" "_TWODOTS_")
      (str/replace #"\." "_DOT_")))

(defn- update-watch! [state watch]
  (p/let [get-editor-data (-> @state :editor/callbacks :editor-data)
          update-watch (-> @state :editor/callbacks :update-watch)
          editor-data (get-editor-data)]
    (when (= (:filename editor-data) (:file watch))
      (update-watch state watch))))

(defn- load-file-cmd [state]
  (p/let [watches (get-all-watches state)
          eql (-> @state :editor/features :eql)
          res (eql [:file/filename :repl/evaluator :editor/contents])
          evaluator (:repl/evaluator res)
          file (:file/filename res)]

    (doseq [{:keys [method]} watches]
      (p/then (eval/evaluate evaluator {:watch_id method} {:plain true :options {:op "unwatch"}})
              #(update-watch! state {:id (to-id method)})))

    (eval/evaluate evaluator (-> res :editor/contents :text/contents) {:filename file})))

(defn- find-definition [state]
  (p/let [dissected (treat/dissect-editor-contents state)
          watch-id (treat/watch-id-for-code state)
          evaluator (treat/eql state :repl/evaluator)
          callee (:callee dissected "self")
          editor-info (p/-> (treat/eql state [{:editor/contents [:file/filename
                                                                 :text/range]}])
                            :editor/contents)
          callee-class (str callee
                            (if (-> dissected :callee-type #{"scope_resolution" "constant"})
                              ".singleton_class"
                              ".class"))
          assign-is-the-expression? (delay (= (:assign/left-side dissected)
                                              (str (:callee dissected)
                                                   (:sep dissected)
                                                   (:identifier dissected))))
          identifier (str (:identifier dissected)
                          (when (-> dissected :type (= "call") (and @assign-is-the-expression?))
                            "="))
          code (case (:type dissected)
                 "call" (str callee-class ".__lazuli_source_location(:" identifier ")")
                 "identifier" (str callee-class ".__lazuli_source_location(:" identifier ")")
                 "constant" (str "Object.const_source_location(" (:identifier dissected) ".name)")
                 "scope_resolution" (str callee ".const_source_location(" (pr-str (:identifier dissected)) ")")
                 nil)]
    (when code
      (p/let [[file row] (eval/evaluate evaluator
                                        (cond-> {:code code
                                                 :file (-> editor-info :file/filename)
                                                 :line (-> editor-info :text/range ffirst)}
                                          watch-id (assoc :eval-opts {:watch_id watch-id}))
                                        {:plain true :options {:op "eval"}})]
        (when (fs/existsSync file)
          {:file file
           :row (dec row)})))))

(defn- goto-definition [state]
  (p/let [definition (find-definition state)]
    (if definition
      (.. js/atom -workspace (open (:file definition)
                                   #js {:initialLine (:row definition)
                                        :searchAllPanes true}))
      ((-> @state :editor/callbacks :notify) {:type :error
                                              :title "Can't find definition"
                                              :message "Can't find definition for the current position"}))))

(defn- add-watch-cmd [state]
  (p/let [{:text/keys [contents range]} (treat/eql state :editor/contents)
          original-row (ffirst range)
          {:keys [method row] :as a} (treat/method-name contents original-row)
          ^js editor (.. js/atom -workspace getActiveTextEditor)
          ^js buffer (.getBuffer editor)
          ^js line (.lineForRow buffer original-row)
          indent (re-find #"^\s+" line)]
    (.setTextInRange buffer
                     (clj->js [[original-row 0] [original-row ##Inf]])
                     (str indent "NREPL.watch!(binding, '" method "')\n" line))))

(defn- deserialize [string]
  (if (str/starts-with? string "[:result")
    {:result (-> string
                 (str/replace "[:result, " "")
                 (str/replace #"]$" "")
                 rp/parse-ruby-string)}
    {:error (let [error-str (-> string
                                (str/replace "[:error, " "")
                                (str/replace #"]$" "")
                                js/JSON.parse)
                  [_ stack] (re-find #" stack=(.*)\>" error-str)
                  error-str (str/replace-first error-str #" stack=(.*)\>" ">")
                  parsed (rp/parse-ruby-string error-str)]
              (if (symbol? parsed)
                (assoc (rp/->RubyUnknownVal (str parsed))
                       :stack (into [] (js/JSON.parse stack)))
                (assoc parsed
                       :stack (into [] (js/JSON.parse stack)))))}))

(defn- get-last-ex [state]
  (p/let [eql (-> @state :editor/features :eql)
          id (str (gensym "last-ex-"))
          editor-info (eql [{:editor/contents [:file/filename
                                               :text/range
                                               :editor/data
                                               :repl/namespace]}])
          editor-info (-> editor-info
                          :editor/contents
                          (dissoc :com.wsscode.pathom3.connect.runner/attribute-errors)
                          (assoc :id id))
          on-start-eval (-> @state :editor/callbacks :on-start-eval)
          _ (on-start-eval state editor-info)
          evaluator (:repl/evaluator @state)
          res (eval/evaluate evaluator {} {:no-wrap true :options {:op "last_exception"}})
          on-eval (-> @state :editor/callbacks :on-eval)
          final-res (-> res
                        (dissoc :result)
                        (assoc :id id)
                        (assoc :error (:error (deserialize (get-in res [:result "result"])))))]
    (on-eval state (with-meta final-res {:tango/error true}))))

(defn- replace-clj-commands [state cmds]
  (let [old-cmds (remove (fn [[k]] (unused-cmds k)) cmds)]
    (concat old-cmds
            {:load-file {:command #(load-file-cmd state)}
             :go-to-var-definition {:command #(goto-definition state)}
             :add-watch-command {:command #(add-watch-cmd state)}
             :get-last-exception {:command #(get-last-ex state)}})))

(defn- norm-command [is-config? editor-data notify k v]
  (let [cmd (:command v)
        curr-lang (:language (editor-data))
        state (state/get-state curr-lang)
        command (some-> state deref (get-in [:editor/commands k]))]

    (cond
      (is-config?)
      ((:command command))

      command
      (cmd)

      state
      (notify {:type :error
               :title "Command not supported"
               :message (str "Command " (name k)
                             " is not supported for " (name curr-lang)
                             " language.")})

      :not-connected
      (notify {:type :error
               :title "Not connected"
               :message (str "REPL not connnected for " (name curr-lang))}))))

#_
(defn- register-commands! [callbacks original-register-commands con cmds state]
  (let [editor-data (:editor-data callbacks)
        lang (:language (editor-data))
        notify (:notify callbacks)
        commands (case lang
                   :ruby (replace-clj-commands state cmds)
                   :clojure cmds)
        is-config? (-> @state :editor/features :is-config?)
        norm-commands (->> commands
                           (map (fn [[k v]] [k (assoc v :command #(norm-command
                                                                   is-config?
                                                                   editor-data
                                                                   notify
                                                                   k v))]))
                           (into {}))]
    (swap! state assoc :editor/commands (into {} commands))
    (original-register-commands con norm-commands state)))

(defn- update-traces [state]
  (let [old-timeout (:repl/tracings-timeout @state)]
    (js/clearTimeout old-timeout)
    (swap! state assoc :repl/tracings-timeout (js/setTimeout #(saphire-console/update-traces! state)
                                                             100))))

(defn- clear-watches! [state]
  (let [con ((:get-console (:editor/callbacks @state)))]
    (set! (.. con (querySelector ".watches") -innerHTML) "")))

(defn- render-watches [state file res]
  (let [watches (get res "watches")]
    (swap! state assoc-in [:repl/watch.info :file] file)
    ((:clear-watches (:editor/features @state)))
    (doseq [watch watches]
      (update-watch! state {:id (to-id (get watch "id"))
                            :text (get watch "id")
                            :icon "icon-eye"
                            :file (get watch "file")
                            :row (get watch "line")}))
    watches))

(defn- display-watches [state file]
  (p/let [evaluator (treat/eql state :repl/evaluator)
          res (eval/evaluate evaluator
                             {:file file}
                             {:plain true :options {:op "get_watches"}})]
    (render-watches state file res)))

(defn- update-watches [state file row delta]
  (p/let [evaluator (treat/eql state :repl/evaluator)
          res (eval/evaluate evaluator
                             {:file file :line row :delta delta}
                             {:plain true :options {:op "update_watch_lines"}})]
    (render-watches state file res)))

(defn remove-watches [state {:keys [file ids]}]
  (let [evaluator (:repl/evaluator @state)]
    (if file
      (p/let [res (eval/evaluate evaluator
                                 {:file file}
                                 {:plain true :options {:op "get_watches"}})]
        (remove-watches state {:ids (->> (get res "watches") (map #(get % "id")) (concat ids))}))
      (doseq [id ids]
        (eval/evaluate evaluator {:watch_id id} {:options {:op "unwatch"}})
        (update-watch! state {:id (to-id id)})))))

(defn- update-resolvers! [state]
  (let [add-pathom-resolvers (-> @state :editor/features :pathom/add-pathom-resolvers)
        remove-resolver (-> @state :editor/features :pathom/remove-resolvers)
        add-resolver (-> @state :editor/features :pathom/add-resolver)
        compose-resolver (-> @state :editor/features :pathom/compose-resolver)]
    (add-pathom-resolvers [treat/doc-for-var treat/current-var treat/completions])))

(defn- serialize [code]
  (let [wrapped (str "begin; res = " code "\n[:result, res]; rescue Exception => e\n"
                     "  [:error, e.inspect.sub(/\\>$/, ' stack=' + e.backtrace.inspect+'>')]\nend")]
    wrapped))

(defn- wrap-with-lang [lang]
  (case lang
    :ruby {:serdes {:serialize serialize :deserialize deserialize}}
    :clojure {}
    {:serdes {:serialize identity :deserialize identity}}))

(defn- make-connection! [lang host port callbacks {:keys [open-console set-console?]}]
  #_
  (p/let [console (atom nil)
          old-register-cmds (:register-commands callbacks)
          {:keys [on-disconnect editor-data]} callbacks
          callbacks (-> {:on-stderr #(tango-console/stderr @console %)
                         :get-console #(deref console)
                         :update-traces update-traces
                         :update-watch saphire-console/update-watch!
                         :repl-options (wrap-with-lang lang)}
                        (merge callbacks)
                        (assoc :register-commands #(register-commands! callbacks old-register-cmds console %1 %2)
                               :on-disconnect (fn [state]
                                                (on-disconnect state)
                                                (swap! state/connections dissoc lang))))
          repl-state (conn/connect! host port callbacks)]
    (when repl-state
      (swap! repl-state update :editor/features merge
             {:display-watches #(display-watches repl-state %)
              :clear-watches #(clear-watches! repl-state)
              :update-watches (fn [file row delta] (update-watches repl-state file row delta))
              :render-watches (fn [file res] (render-watches repl-state file res))
              :find-definition #(find-definition repl-state)
              :result-for-renderer (case lang
                                     :ruby ui-ruby/for-result
                                     edn/for-result)
              :remove-watches #(remove-watches repl-state %)})
      (swap! repl-state assoc :repl/tracings [] :text/language lang)
      (update-resolvers! repl-state)
      (swap! state/connections assoc lang repl-state)

      (p/then (open-console repl-state)
              (fn [c]
                (when (and set-console? (not= :clojure lang))
                  (saphire-console/prepare! c repl-state))
                (reset! console c)
                repl-state)))))

#_
(defn connect-nrepl! [host port callbacks console-info]
  (let [lang (:language ((:editor-data callbacks)))]
    (when-not (:Config @state/connections)
      (-> state/connections
          (swap! assoc :Config (conn/connect-config! callbacks))
          :Config
          (swap! #(-> %
                      (assoc :text/language :Config)
                      (assoc-in [:editor/features :result-for-renderer] edn/for-result)))))
    (make-connection! lang host port callbacks console-info)))
